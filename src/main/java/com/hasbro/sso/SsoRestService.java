package com.hasbro.sso;


import com.hasbro.sso.config.AppConfig;
import com.hasbro.sso.rest.BaseResource;
import io.dropwizard.Application;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.server.DefaultServerFactory;
import io.dropwizard.setup.Environment;
import org.springframework.context.ApplicationContext;

public class SsoRestService extends Application<AppConfig> {

    public static void main(String[] args) throws Exception {
        new SsoRestService().run(args);
    }

    @Override
    public void run(AppConfig appConfig, Environment environment) throws Exception {
        configureJersey(environment.jersey(), appConfig);
    }

    private void configureJersey(JerseyEnvironment jersey, AppConfig config) {
        ((DefaultServerFactory) config.getServerFactory()).setJerseyRootPath("/rest/*");
        jersey.register(new BaseResource());

        //ToDo OTDS Auth
    }
}